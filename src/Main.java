import extensions.*;

class Main extends Program{

	/// FONCTIONS DE JEU

	ListJoueurs joueurs;
	ListCartes toutesLesCartes;
	ListCartes boutique;
	int tour;

	// Procédure principale du programme

	void algorithm(){
		boolean fini=false;
		do{
			String entree;
			do{
				resetScreen();
				println(ANSI_CYAN + ANSI_BOLD + " ______   __             ___    ___           ___               _                 \n" +
				"|_   _ \\ [  |          .' ..] .' ..]        .'   `.            (_)                \n" +
				"  | |_) | | | __   _  _| |_  _| |_   .--.  /  .-.  \\  __   _   __   ____   ____   \n" +
				"  |  __'. | |[  | | |'-| |-''-| |-'/ .'`\\ \\| |   | | [  | | | [  | [_   ] [_   ]  \n" +
				" _| |__) || | | \\_/ |, | |    | |  | \\__. |\\  `-'  \\_ | \\_/ |, | |  .' /_  .' /_  \n" +
				"|_______/[___]'.__.'_/[___]  [___]  '.__.'  `.___.\\__|'.__.'_/[___][_____][_____]" + ANSI_RESET);
				println("\n" + ANSI_BLUE + ANSI_BOLD + "Par Gireg Lezoraine et Hugo Briatte" + ANSI_RESET + "\n\nQue voulez vous faire?\nEntrez " + 
					ANSI_RED + ANSI_BOLD + " R " + ANSI_RESET + " pour lire les régles\nEntrez " + 
					ANSI_RED + ANSI_BOLD + " J " + ANSI_RESET + " pour jouer\nEntrez " + 
					ANSI_RED + ANSI_BOLD + " Q " + ANSI_RESET + " pour quitter\n\n" + 
					ANSI_GREEN + ANSI_BOLD + "Appuyez sur ENTREE pour valider votre proposition" + ANSI_RESET);
				entree=toUpperCase(readString());
			} while( !equals(entree, "J") && !equals(entree, "R") && !equals(entree, "Q"));
			if (equals(entree, "J")){
				partie();
			} else if (equals(entree, "R")) {
				regles();
			} else{
				fini = true;
				resetScreen();
			}	
		} while(!fini);
	}

	// Procedure pour afficer les regles du jeu au joueur

	void regles(){
		String[] reglesCoupe =new String[] {"Bienvenue dans BluffoQuizz, un jeu se jouant de 3 à 5 joueurs.\nLe but du jeu est de gagner 15 points le plus rapidement possible. Vous avez 10 tour de jeu pour cela.", "Chacun des joueurs possèdent au début de la partie 3 cartes questions et 10 points. Ces questions portent sur de nombreuse matières que tu étudies en CM2. Chacun votre tour, vous allez pouvoir:\n-Prendre une nouvelle carte dans une boutique contenant 3 cartes questions\n-Utilser une carte question sur quelqu'un.", "Lorsque tu utilise une carte question sur quelqu'un, celui-ci va devoir répondre a la question soit en choisisant la bonne reponse parmis des propositions (c'est un qcm) ou bien en entrant manuellement la réponse.", "Si celui qui a reçu la carte reponds correctement, il gagne un point et toi tu en perds un. Si il reponds mal, tu gagnes un point et il en perds un."};
		for (int i = 0; i < length(reglesCoupe); i++) {
			cursor(0, 0);
			clearScreen();
			println(reglesCoupe[i]);
			continuer();
		}
	}

	// Procedure qui fait tourner une partie

	void partie(){
		// Choix difficulté
		String entree;
		do{
			resetScreen();
			println("Quel mode de difficulté choissisez vous?\n1\t-\tFacile\n2\t-\tMoyen\n3\t-\tDifficile\nEntrez le nombre correspondant puis appyez sur ENTREE");
			entree = readString();
		} while(!estInt(entree) || motToInt(entree) < 1 || motToInt(entree) > 3);
		toutesLesCartes = trierCategorieCarte(genererCartes(loadCSV("cartes.csv", ';')), CategorieCarte.DIFFICULTE, entree);
		boutique = creerListCartes();
		joueurs = entreeJoueurs();
		// Creation de deck au hasard pour les joueurs et la boutique
		for (int i = 0; i < length(joueurs); i++) {
			for (int j = 0; j < 3; j++) {
				Carte haz;
				do {
					haz = random(toutesLesCartes);
				} while(contains(haz, get(joueurs, i).cartes));
				add(get(joueurs, i).cartes, haz);
			}
		}
		for (int i = 0;i < 3; i++) {
			add(boutique, random(toutesLesCartes));
		}
		tour = 1;
		// Boucle de tour de partie
		while (tour <= 10 && !partieFini()) {
			clearScreen();
			cursor(0, 0);
			println(ANSI_RED + ANSI_BOLD + "TOUR N°" + tour + "\n\n" + ANSI_RESET);
			continuer();
			for (int j = 0; j < length(joueurs); j++) {
				if (!partieFini()) {
					jouer(get(joueurs,j));
				}
			}
			tour = tour + 1;
		}
		finPartie();
	}

	// Fonction qui verifie si une partie est finie ou non

	boolean partieFini() {
		int idx = 0;
		while (idx < length(joueurs)) {
			if (get(joueurs, idx).points >= 15) {
				return true;
			}
			idx = idx + 1;
		}
		return false;
	}

	// Fonction qui affiche le message de fin

	void finPartie(){
		String nomGagnant="";
		int idx = 0;
		int scoreMax = 0;
		while (idx < length(joueurs)) {
			if (get(joueurs, idx).points > scoreMax) {
				nomGagnant=get(joueurs,idx).nom;
				scoreMax = get(joueurs,idx).points;
			}else if(get(joueurs, idx).points == scoreMax){
				nomGagnant = nomGagnant + " et "+get(joueurs,idx).nom;
			}
			idx = idx + 1;
		}
		println("Et le gagnant est "+nomGagnant+". Bravo a vous tous!");
		continuer();

	}

	// Procedure pour afficher les informations sur les scores et les cartes d'un joueur

	void affichageJouer(Joueur joueur) {
		resetScreen();
		println(ANSI_RED + ANSI_BOLD + "TOUR N°" + tour + ANSI_RESET + "\n\nAu tour de " + joueur.nom + "\n\n");
		printScoreboard();
		println("\n\n" + affichageCarte(joueur.cartes, 12));
	}

	// Procedure pour lancer le tour d'un joueur

	void jouer(Joueur joueurActuel) {
		if (!joueurActuel.estIa) {
			String choix = "";
			do {
				affichageJouer(joueurActuel);
				println("\n\nQue voulez vous faire?\nEntre B pour aller dans la boutique\nEntre U pour utiliser une carte\n\n" + ANSI_GREEN + ANSI_BOLD + "Appuyez sur ENTREE pour valider votre réponse" + ANSI_RESET);
				choix = toUpperCase(readString());
			} while(!equals(choix, "B") && !equals(choix, "U"));
			if (equals(choix, "B")) {
				acheterCarte(joueurActuel);
			} else if (length(joueurActuel.cartes) > 0) {
				jouerCarte(joueurActuel);
			} else {
				jouer(joueurActuel);
			}
		} else {
			affichageJouer(joueurActuel);
			chargement(joueurActuel);
			if (random() <= 0.5 || length(joueurActuel.cartes) == 0) {
				println(joueurActuel.nom + " va dans la boutique");
				continuer();
				acheterCarte(joueurActuel);
			} else {
				println(joueurActuel.nom + " décide de jouer une carte");
				continuer();
				jouerCarte(joueurActuel);
			}
		}
	}

	// Procedure pour qu'un joueur utilise une carte

	void jouerCarte(Joueur joueurActuel) {
		if (!joueurActuel.estIa) {
			String entree;
			do {
				resetScreen();
				affichageJouer(joueurActuel);
				println("\n\nQuel carte veux-tu jouer?");
				entree = readString();
			} while (!estInt(entree) || motToInt(entree) <= 0 || motToInt(entree) > length(joueurActuel.cartes));
			joueurActuel.carteChoisie = get(joueurActuel.cartes, motToInt(entree) - 1);
			remove(joueurActuel.cartes, motToInt(entree) - 1);
			Joueur[] listNumerote = numeroter(joueurs, joueurActuel);
			do {
				resetScreen();
				println(ANSI_RED + ANSI_BOLD + "TOUR N°" + tour + ANSI_RESET + "\n\nAu tour de " + joueurActuel.nom + "\n\n");
				printScoreboard();
				println("A qui veux tu donner la carte avec la question : " + joueurActuel.carteChoisie.contenu);
				for (int i = 0; i < length(listNumerote); i++) {
					println((i + 1) + "\t" + listNumerote[i].nom);
				}
				entree = readString();
			} while(!estInt(entree) || motToInt(entree) <= 0 || motToInt(entree) > length(listNumerote));
			joueurActuel.cible = listNumerote[motToInt(entree) - 1];
			println(joueurActuel.cible.nom + ". Tu dois repondre a cette question");
			continuer();
		} else {
			definirJoueurCibleIA(joueurActuel);
			definirCarteIA(joueurActuel);
			println(joueurActuel.nom + " a choisi la carte : " + joueurActuel.carteChoisie.contenu + "\n" + joueurActuel.nom + " s'attaque a : " + joueurActuel.cible.nom + "\n" + joueurActuel.cible.nom + " va devoir répondre a cette question.");
			continuer();
		}
		poserQuestion(joueurActuel);
	}

	// Fonction qui retourne un tableau de tout les joueurs sauf un

	Joueur[] numeroter(ListJoueurs list, Joueur joueurActuel) {
		Joueur[] res = new Joueur[length(list)-1];
		int i = 0;
		int j = 0;
		while (i < length(list)) {
			if (get(list, i) == joueurActuel) {
				i = i + 1;
			}
			if (i < length(list)) {
				res[j] = get(list, i);
			}
			i = i + 1;
			j = j + 1;
		}
		return res;
	}

	// Procedure pour afficher la boutique

	void affichageBoutique(Joueur j) {
		resetScreen();
		println(ANSI_RED + ANSI_BOLD + "TOUR N°"+ tour + ANSI_RESET + "\n\nVoici les cartes disponibles dans la boutique:\n\n" + affichageCarte(boutique, 12));
	}

	// Procedure pour qu'un joueur achete une carte

	void acheterCarte(Joueur j) {
		affichageBoutique(j);
		int numCarte = 0;
		boolean quitter = false;
		if (!j.estIa) {
			String entree;
			do {
				println("\n\nSi vous voulez prendre une carte, marquer le numero correspondant.\nSi vous voulez faire une autre action, entrez Q.");
				entree = toUpperCase(readString());
			} while(!equals(entree, "Q") && !equals(entree, "1") && !equals(entree, "2") && !equals(entree, "3"));
			if (equals(entree, "Q")) {
				jouer(j);
				quitter = true;
			} else {
				add(j.cartes, get(boutique, motToInt(entree)-1));
				remove(boutique, motToInt(entree)-1);
				add(boutique, random(toutesLesCartes));
				println("Vous avez pris la carte N°" + entree + ".");
			}
		} else {
			int carte = (int) (random()*3);
			add(j.cartes, get(boutique, carte));
			remove(boutique, carte);
			add(boutique, random(toutesLesCartes));
			println(j.nom + " a pris la carte N°" + (carte + 1));
		}
		if (!quitter) {
			continuer();
		}
	}

	// Procedure pour entree les joueurs en debut de partie
	
	ListJoueurs entreeJoueurs() {
		int nbrJoueurHum;
		String entree;
		int nbrJoueursMax = 5;
		do {
			resetScreen();
			println("Combien de joueurs (hors ordinateur) vont jouer? (Maximum 5 joueurs)");
			entree=readString();
		} while(!estInt(entree) || motToInt(entree) < 0 || motToInt(entree) > nbrJoueursMax);
		nbrJoueurHum = motToInt(entree);
		nbrJoueursMax = 5 - nbrJoueurHum;
		ListJoueurs listJ = creerListJoueurs();
		for (int i = 1; i <= nbrJoueurHum; i++) {
			println("Entre le nom du joueur N°" + i);
			String nom = "";
			boolean present;
			do {
				if (nom != "") {
					println("Entre un autre nom!");
				}
				nom = readString();
				int j = 0;
				present = false;
				while(j < length(listJ) && !present){
					if(equals(get(listJ, j).nom, nom)){
						present = true;
					}
					j = j + 1;
				}
			} while(present || length(nom) == 0);
			add(listJ, creerJoueur(nom, creerListCartes(), 10));
			get(listJ, i - 1).competences = creerListCompetencesInitiale();
		}
		if (nbrJoueursMax != 0) {
			do {
				resetScreen();
				println("Combiens d'ordinateurs ? (Minimum " + max(0, nbrJoueursMax - 2) + ", Maximum " + nbrJoueursMax + ")");
				entree = readString();

			} while (!estInt(entree) || motToInt(entree) < max(0, nbrJoueursMax - 2)  || motToInt(entree) > nbrJoueursMax);
			int nbrIas = motToInt(entree);

			for (int i = 1; i <= nbrIas; i++) {
				println("Entre le nom de l'ordinateur N°" + i);
				String nom = "";
				boolean present;
				do {
					if (nom != "") {
						println("Entre un autre nom!");
					}
					nom = readString();
					int j = 0;
					present = false;
					while(j < length(listJ) && !present){
						if (equals(get(listJ, j).nom, nom)) {
							present = true;
						}
						j = j + 1;
					}
				} while(present || length(nom)==0);
				add(listJ, creerIA(nom, creerListCartes(), 10));
				Joueur iaAjoute=get(listJ, nbrJoueurHum + i - 1);
				iaAjoute.competences = creerListCompetencesInitiale();
				for(int j = 0; j < length(iaAjoute.competences); j++){
					get(iaAjoute.competences,j).taux=random();
				}
			}
		}

		return listJ;
	}

	// Procedure pour qu'une IA choisisse une carte a jouer

	void definirCarteIA(Joueur ia) {
		ListCartes cartesPossible = trierCategorieCarte(ia.cartes, CategorieCarte.MATIERE, competenceMin(ia.cible, ia.cartes));
		int numero = (int) (random() * length(cartesPossible));
		ia.carteChoisie = get(cartesPossible, numero);
		remove(ia.cartes, ia.carteChoisie);
	}

	// Procedure pour enlever d'une liste de carte une carte specifique
	
	void testRemoveCarte(){
		assertTrue(true);
	}

	void remove(ListCartes list, Carte carte){
		if (equals(list.carte, carte)) {
			remove(list, 0);
		}else if(list!=null){
			remove(list.suivante, carte);
		}
	}

	// Procedure pour qu'une IA choisisse un joueur a qui donner une carte

	void definirJoueurCibleIA(Joueur ia) {
		Joueur[] joueursPossibles = numeroter(joueurs, ia);
		int factor = (int) (random() * (length(joueursPossibles)));
		ia.cible = joueursPossibles[factor];
	}

	// Procedure pour poser une question a un joueur
	
	void poserQuestion(Joueur joueur) {
		Joueur cible = joueur.cible;
		Carte carte = joueur.carteChoisie;
		if (askQuestion(carte,cible)) {
			println("\nBravo !\n\n");
			cible.points += 1;
			joueur.points -= 1;
			if(!cible.estIa){
				find(cible.competences, carte.matiere).taux += 0.1;
			}
		} else {
			println("\nDommage !\n\n");
			cible.points -= 1;
			joueur.points += 1;
			if(!cible.estIa){
				find(cible.competences, carte.matiere).taux -= 0.1;
			}
		}
		println("La bonne réponse est :\n\n" + carte.reponses[0] + "\n\n" + carte.explication + "\n\n");
		continuer();
		resetScreen();
		println(ANSI_BLUE + "Score : \n\n" + ANSI_RESET);
		printScoreboard();
		println("\n\n");
		continuer();
	}

	// Fonction qui donne une question a un joueur et dit si il a repondu correctement ou non

	boolean askQuestion(Carte c, Joueur j){
		resetScreen();
		if (!j.estIa) {
			if (equals(c.mode, "qcm")) {
				return askQcm(c.contenu, c.reponses);
			} else if (equals(c.mode, "string")) {
				return askString(c.contenu, c.reponses[0]);
			} else if (equals(c.mode, "int")) {
				return askInt(c.contenu, motToInt(c.reponses[0]));
			}
			return askDouble(c.contenu, motToDouble(c.reponses[0]));
		} else {
			println(c.contenu);
			String[] choices = c.reponses;
			if (equals(c.mode, "qcm")) {
				int[] tabNombreHaz = tabNombreHaz(choices);
				for (int i = 0; i < length(choices); i++) {
					println("\t" + (i + 1) + " - " + choices[tabNombreHaz[i]]);
				}
			}
			chargement(j);
			if (random() < taux(j, c.matiere)) {	
				println(j.nom + " choisi la réponse : " + choices[0]);
				continuer();
				return true;
			}else{
				println(j.nom + " choisi la réponse : " + choices[(int) (random() * (length(choices) - 1) + 1)]);
				continuer();
				return false;
			}
		}
	}
	
	// Question de type entrée double
	
	boolean askDouble(String contenu, double correctAnswer) {
		String sentAnswer;
		do{
			resetScreen();
			println(contenu + "\nVous devez rentrer un nombre");
			sentAnswer=readString();
		} while (estDouble(sentAnswer) == -1);
		if (motToDouble(sentAnswer) == correctAnswer) {
			return true;
		} else {
			return false;
		}
	}

	// Question de type entrée int
	
	boolean askInt(String contenu, int correctAnswer) {
		String sentAnswer;
		do {
			resetScreen();
			println(contenu + "\nVous devez rentrer un nombre entier");
			sentAnswer = readString();
		} while(!estInt(sentAnswer));
		if (motToInt(sentAnswer) == correctAnswer) {
			return true;
		} else {
			return false;
		}
	}

	// Question de type entrée string
	
	boolean askString(String contenu, String correctAnswer) {
		println(contenu);
		String sentAnswer;
		do{
			sentAnswer=toUpperCase(readString());
		}while(length(sentAnswer)==0);
		if (equals(sentAnswer, toUpperCase(correctAnswer))) {
			return true;
		} else {
			return false;
		}
	}

	// Question de type QCM
	
	boolean askQcm(String contenu, String[] choices) {
		int[] tabNombreHaz = tabNombreHaz(choices);
		int sentAnswer;
		String entree;
		do {
			resetScreen();
			println(contenu);
			for (int i = 0; i < length(choices); i++) {
				println("\t" + (i + 1) + " - " + choices[tabNombreHaz[i]]);
			}
			print("Entrez le numero correspondant a votre reponse : ");
			entree = readString();
		} while(!estInt(entree) || motToInt(entree) < 1 || motToInt(entree) > length(choices));
		sentAnswer = motToInt(entree);
		if (tabNombreHaz[sentAnswer - 1] == 0) {
			return true;
		} else {
			return false;
		}
	}
	
	// FONCTION D'ENTREE DE DONNEES

	// Fonction qui verifie si un caractère représente un chiffre
	
	void testEstChiffre(){
		assertTrue(estChiffre('1'));
		assertFalse(estChiffre('.'));
		assertTrue(estChiffre('9'));
	}

	boolean estChiffre(char c) {
        	return ((int) c >= (int) '0') && ((int) c <= (int) '9');
    	}

	// Fonction qui transforme un string convertible en float
	
	void testMotToDouble(){
		assertEquals(7.24, motToDouble("007,24000"));
		assertEquals(-34.98, motToDouble("-0034,9800"));
		assertEquals(0.98, motToDouble(".980"));
		assertEquals(984.0, motToDouble("984."));
		assertEquals(12.0, motToDouble("12"));
	}

	double motToDouble(String entree) {
		int debut = 0;
		double res = 0;
		if (estInt(entree)) {
			res = motToInt(entree);
		} else {
			if(charAt(entree, 0) == '-'){
				debut = 1;
			}
			int dernierChiffre = debut;
			for (int i = debut; i < length(entree); i++) {
				if (estChiffre(charAt(entree, i))){
					res = res * 10 + (float) (charAt(entree, i) - '0');
					dernierChiffre = i;
				} else if (length(entree) == i + 1) {
					dernierChiffre = i;
				}else{
					dernierChiffre = i + 1;
				}
			}
			res=res*pow(10, -dernierChiffre + estDouble(entree));
			if(debut == 1){
				res = res * (-1);
			}
		}
		return res;
	}

	// Fonction qui verifie si un String peut être converti en float: SI oui, renvoie la position du point ou de la virgule, sinon retourne -1 
	
	void testEstDouble(){
		assertEquals(2,estDouble("12,45"));
		assertEquals(1,estDouble("3,"));
		assertEquals(-1,estDouble("34L3"));
		assertEquals(-1,estDouble("45..4"));
		assertEquals(1,estDouble("-,"));
		assertEquals(0,estDouble(".0"));
	}
	
	int estDouble(String mot){
		boolean res = true;
		int idx = 0;
		if(length(mot) == 0){
			res = false;
		}else if(charAt(mot, 0) == '-'){
			idx = 1;
			if(length(mot) == 1){
				res = false;
			}
		}
		int compteurPoint = length(mot);
		while(res && idx < length(mot)){
			if(charAt(mot, idx) == ',' || charAt(mot, idx) == '.'){
				if(compteurPoint == length(mot)){
					compteurPoint = idx;
				}else{
					res = false;
				}
			}else if(!estChiffre(charAt(mot, idx))){
				res = false;
			}
			idx = idx + 1;
		}
		if(!res){
			compteurPoint = -1;
		}
		return compteurPoint;
	}
	
	// Fonction qui convertis un String compatible en Int
	
	void testMotToInt(){
		assertEquals(-42, motToInt("-42"));
		assertEquals(98, motToInt("98"));
		assertEquals(54060 ,motToInt("0054060"));
	}
	
	int motToInt(String entree){
		int debut = 0;
		int res = 0;
		if(charAt(entree, 0) == '-'){
			debut = 1;
		}
		for(int i = debut; i < length(entree); i++){
			res = res * 10 + (int) (charAt(entree, i) - '0');
		}
		if(debut == 1){
			res = res * (-1);
		}
		return res;

	}
	
	// Fonction qui verifie si un String peut être converti en int
	
	void testEstInt(){
		assertTrue(estInt("-59"));
		assertTrue(estInt("89452"));
		assertFalse(estInt("-"));
		assertFalse(estInt("zsf"));
	}
	
	boolean estInt(String mot){
		int idx = 0;
		if(length(mot) == 0){
			return false;
		}else if(charAt(mot, 0) == '-'){
			idx = 1;
			if(length(mot) == 1){
				return false;
			}
		}
		while(idx < length(mot)){
			if(!estChiffre(charAt(mot, idx))){
				return false;
			}
			idx = idx + 1;
		}
		return true;
	}

	// FONCTIONS UTILITAIRES

	// Affiche du texte donnant l'illusion qu'une IA reflechit

	void chargement(Joueur ia) {
		print(ia.nom); delay(750); print(" . "); delay(750); print(" . "); delay(750); print(" .\n");
	}

	// Demande au joueur de confirmer pour continuer

	void continuer() {
		println(ANSI_GREEN + ANSI_BOLD + ANSI_BLINK_SLOW + "Appuyez sur ENTREE pour continuer" + ANSI_RESET);
		readString();
	}

	// Efface l'ecran et place le curseur tout en haut a gauche

	void resetScreen() {
		clearScreen(); cursor(1, 1);
	}

	// Retourne le texte qui est le plus grand en terme de caractère au joueur
	
	void testlongeurPhraseLaPlusGrande(){
		assertEquals(6,longeurPhraseLaPlusGrande(new String[]{"La","Plus","Grande"}));
		assertEquals(10,longeurPhraseLaPlusGrande(new String[]{"Pomme","Clementine","Raisin","Orange"}));
	}
	
	int longeurPhraseLaPlusGrande(String[] listString){
		if(length(listString) != 0){
			int res=length(listString[0]);
			for(int i = 0;i < length(listString);i++){
				res=max(res,length(listString[i]));
			}
			return res;
		}else{
			return 0;
		}
    	}

	// Supprimes les espaces avant un string
	
	void testSupprimerEspaceAvant(){
		assertEquals("hey ca va? ",supprimerEspaceAvant("  hey ca va? "));
		assertEquals("", supprimerEspaceAvant("   "));
	}
	
	String supprimerEspaceAvant(String phrase){
		if(length(phrase) == 0 || charAt(phrase, 0) != ' '){
			return phrase;
		}
		return supprimerEspaceAvant(substring(phrase, 1, length(phrase)));
	}
	
	// Retourne l'index du dernier espace entre le caractère 0 et le n-eme caractère ou le n+1eme caractère si ce dernier ets un espace
	
	void testFinDernierMot(){
		assertEquals(2,finDernierMot("Ou Anticonstitutionellement Peut-être",4));
		assertEquals(4,finDernierMot("Minority Report",5));
	}
	
	int finDernierMot(String phrase, int longeur){
		int res=min(longeur, length(phrase)) - 1;
		for(int i=0 ; i<min(longeur+1, length(phrase)) ; i++){
			if(charAt(phrase,i) == ' '){
				res = i;
			}
		}
		return res;
	}

	// Retourne un texte coupé en un certaine nombre de partie de caractère n*longeur. Chaque partie est faite de tel manière que les mots soient evités d'être coupé
	
	void testCouperEtCentrer(){
		assertEquals("We  got the power tobe  loving  eachothe r  ",couperEtCentrer("We got the power to be loving each other",4));
		assertEquals("If I  could take  herdown  andrun,  then  I'dcall  her",couperEtCentrer("If I could take her down and run, then I'd call her",3));
	}
	
	String couperEtCentrer(String phrase, int longeur){
		String phraseFinale="";
		while(length(phrase) > longeur){
			int finDernierMot = finDernierMot(phrase, longeur);
			String phraseCoupe = substring(phrase, 0, finDernierMot + 1);
			phraseCoupe = textAlign(longeur, phraseCoupe,Side.CENTER);
			phraseFinale = phraseFinale + phraseCoupe;
			phrase = substring(phrase, finDernierMot + 1, length(phrase));
			phrase = supprimerEspaceAvant(phrase);
		} 
		phraseFinale = phraseFinale + textAlign(longeur, phrase,Side.CENTER);
		return phraseFinale;
	}

	// Retourne un texte qui peut être aligné sur la gauche, la droite ou le centre et prendra une certaine longeur donnée

	void testTextAlign(){
		assertEquals("Un peu de texte ",textAlign(16,"Un peu de texte",Side.LEFT));
		assertEquals("  Oui et toi?",textAlign(13,"Oui et toi?",Side.RIGHT));
		assertEquals(" Nickel ",textAlign(8,"Nickel",Side.CENTER));
	}
	
	String textAlign(int width, String text, Side side) {
                String result = "";
		int maxLength = length(text);
		int spaceAmount = 0;

		switch (side) {
			case LEFT:
                        	if (length(text) < width) spaceAmount = width - length(text);
                        	else if (length(text) > width) maxLength = width;
            
                        	for (int i = 0; i < maxLength; i++) result = result + charAt(text, i);
                        	for (int i = 0; i < spaceAmount; i++) result = result + " ";
			break;
			case CENTER:
				int spaceLeft = 0;
                        	int spaceRight = 0;
                        	if (length(text) < width) spaceLeft = (int) ((width - length(text)) / 2) ;
                        	else if (length(text) > width) maxLength = width;

                        	while (spaceLeft + spaceRight + length(text) < width) spaceRight = spaceRight + 1;

                        	for (int i = 0; i < spaceLeft; i++) result = result + " ";
                        	for (int i = 0; i < maxLength; i++) result = result + charAt(text, i);
                        	for (int i = 0; i < spaceRight; i++) result = result + " ";
			break;
			case RIGHT:
                        	if (length(text) < width) spaceAmount = width - length(text);
                        	else if (length(text) > width) maxLength = width;

                        	for (int i = 0; i < spaceAmount; i++) result = result + " ";
                        	for (int i = 0; i < maxLength; i++) result = result + charAt(text, i);
			break;
			default:
				result = result + "Alignment Error";
		}
        	return result;
    	}

	// Retourne un tableau composé d'int de 0 a la longeur d'un tableau donnée
	// Utilisé pour afficher les réponses au hasard
	
	int[] tabNombreHaz(String[] reponses){
		int[] res = new int[length(reponses)];
		for(int i = 0; i<length(res); i++){
			res[i] = i;
		}
		for(int i = 0; i<length(res); i++){
			int tmp = res[i];
			int nbHaz = (int)(random() * length(res));
			res[i] = res[nbHaz];
			res[nbHaz] = tmp;
		}
		return res;
	}

	// Fonction pour créer un tableau de String a partir d'un String content des mots ou phrases separées par des tubes '|'

	void testSplitReponsesCartes(){
		assertArrayEquals(new String[]{"Un","Deux","Trois"},splitReponsesCartes("Un|Deux|Trois"));
		assertArrayEquals(new String[]{""},splitReponsesCartes(""));
		assertArrayEquals(new String[]{"Heyo"},splitReponsesCartes("Heyo"));
	}

	String[] splitReponsesCartes(String line) {
			char separator = '|';
			int count = 1;
			for (int i = 0; i < length(line); i++) {
					if (charAt(line, i) == separator) count = count + 1;
			}
			String[] result = new String[count];
			int index = 0;
			String tmp = "";
			for (int i = 0; i < length(line); i++) {
					if (charAt(line, i) == separator) {
							result[index] = tmp;
							tmp = "";
							index = index + 1;
					} else tmp = tmp + charAt(line, i);
			}
			result[index] = tmp;

			return result;
	}

	// FONCTIONS D'AFFICHAGE

	// Affiche les scores
	
	void printScoreboard() {
		print(ANSI_GREEN + ANSI_BOLD);
		for (int i = 0; i < 50; i++) {
			print("#");
		}
		println("");
		print(ANSI_RESET + ANSI_BLUE  + ANSI_BOLD);
		for (int i = 0; i < length(joueurs); i++) {
			println(textAlign(50, textAlign(24, "Joueur " + get(joueurs,i).nom, Side.LEFT) + textAlign(24, textAlign(8, "Score : ", Side.LEFT) + textAlign(4, "" + get(joueurs,i).points, Side.RIGHT), Side.RIGHT), Side.CENTER));
		}
		print(ANSI_RESET + ANSI_GREEN + ANSI_BOLD);
		for (int i = 0; i < 50; i++){
			print("#");
		}
		print(ANSI_RESET);
		println("");
	}

	void testCreationSectionCarte(){
		assertEquals("┃Quel est le┃Quel est la┃\n┃ président ┃ valeur de ┃\n┃de France? ┃    pi?    ┃\n",creationSectionCarte(new String[]{"Quel est le président de France? ","Quel est la valeur de     pi?    "},11));
	}

	String creationSectionCarte(String[] textes,int longeur){
		String affichage = "";
		for (int i = 0; i < longeurPhraseLaPlusGrande(textes) / longeur;i++){
	    		affichage = affichage+'┃';
			for (int j = 0; j < length(textes); j = j+1){
				if(length(textes[j]) > i * longeur){
					affichage = affichage+substring(textes[j], i * longeur, longeur * (i + 1));
				}else{
					for(int k = 0; k < longeur; k++){
						affichage = affichage + ' ';
					}
				}
	    			affichage = affichage + '┃';
			}
	    		affichage = affichage + '\n';

		}
		return affichage;
	}

	// Créer une ligne qui peut être une entête, une intersection ou un bas de carte pour l'affichage des cartes
	
	void testCreationIntersection(){
		assertEquals("[━━━+━━━+━━━]\n",creationIntersection('[','+',']',3,3));
		assertEquals("{━━|━━}\n",creationIntersection('{','|','}',2,2));
	}

	String creationIntersection(char g,char m, char d, int longeurList, int longeurCarte){
		String affichage = "";
		for(int i = 0; i <= longeurList * (longeurCarte + 1); i++){
			if(i == 0){
				affichage = affichage + g;
			}else if(i == longeurList * (longeurCarte + 1)){
				affichage = affichage + d + '\n';
			}else if(i % (longeurCarte + 1) == 0){
				affichage = affichage + m;
			}else{
	    			affichage = affichage + '━';
			}
		}
		return affichage;
	}

	// Fonctions qui retourne un String affichants une liste de carte ou les cartes d'un joueur.

	String affichageCarte(Joueur j,int longeur){
		return "Voici tes cartes " + j.nom + '\n' + affichageCarte(j.cartes,longeur);
	}

	String affichageCarte(ListCartes cartes,int longeur){
		if(length(cartes) > 0){
			String affichage = "";
			String[] numeros = new String[length(cartes)];
			String[] matieresCoupe = new String[length(cartes)];
			String[] mode = new String[length(cartes)];
			String[] contenusCoupe = new String[length(cartes)];
			for(int i = 0; i < length(cartes); i++){
				numeros[i] = couperEtCentrer(""+(i+1), longeur);
				matieresCoupe[i] = couperEtCentrer(get(cartes,i).matiere, longeur);
				contenusCoupe[i] = couperEtCentrer(get(cartes,i).contenu, longeur);
				if(equals(get(cartes, i).mode, "qcm")){
					mode[i] = couperEtCentrer("QCM",longeur);
				}else{
					mode[i] = couperEtCentrer("Entree libre",longeur);
				}
			}
			affichage = affichage + creationIntersection('┏','┳','┓', length(cartes), longeur);
			affichage = affichage + creationSectionCarte(numeros, longeur);
			affichage = affichage + creationIntersection('┣','╋','┫', length(cartes), longeur);
			affichage = affichage + creationSectionCarte(matieresCoupe, longeur);
			affichage = affichage + creationIntersection('┣','╋','┫', length(cartes), longeur);
			affichage = affichage + creationSectionCarte(mode, longeur);
			affichage = affichage + creationIntersection('┣','╋','┫', length(cartes), longeur);
			affichage = affichage + creationSectionCarte(contenusCoupe, longeur);
			affichage = affichage + creationIntersection('┗','┻','┛', length(cartes), longeur);

			return affichage;
		}
		return "\n\nAucune Carte";
	}

	// FONCTIONS SUR LES TABLEAUX

	// Verifie si deux tableaux de String sont egaux

	void testTableauStringEgales(){
		assertTrue(equals(new String[]{"A","C","V"},new String[]{"A","C","V"}));
		assertFalse(equals(new String[]{"C","V"},new String[]{"C","Z"}));
		assertFalse(equals(new String[]{"C","V"},new String[]{"C"}));
	}

	boolean equals(String[] s1,String[] s2){
		if(length(s1) == length(s2)){
			int i = 0;
			while(i < length(s1)){
				if(! equals(s1[i], s2[i])){
					return false;
				}
				i = i+1;
			}
		}else{
			return false;
		}
		return true;
	}

	// FONCTIONS SUR LES CARTES

	// Verifie si une carte est présente dans un tableau de carte

	void testContains(){
		ListCartes l=creerListCartes();
		Carte c=creerCarte(1,"qcm","Francais","Quand a débuté la 2nd Guerre Mondiale?",new String[]{"1939","1938","1937","1940"},"La 2nd Guerre Mondiale à débuté officiellement en 1939, même si certains évènements précédants 1939 l'ont annoncé auparavant.");
		add(l,c);
		assertTrue(contains(c,l));
		assertFalse(contains(creerCarte(1,"qcm","Francais","Quand a débuté la 1ere Guerre Mondiale?",new String[]{"1914","1913","1915","1916"},"Le debut de la 1er Guerre Mondiale date de 1914 en Europe. Pus tard, en 1917, les Etats Unis d'amérique les rejoindrons"),l));

	}

	boolean contains(Carte carte,ListCartes list){
		while(list.carte != null){
			if(equals(carte, list.carte)){
				return true;
			}
			list = list.suivante;
		}
		return false;
	}

	// Verifie si deux cartes sont egaux

	void testEqualsCarte(){
		Carte c1=creerCarte(1,"qcm","Francais","Quand a débuté la 2nd Guerre Mondiale?",new String[]{"1939","1938","1937","1940"},"La 2nd Guerre Mondiale à débuté officiellement en 1939, même si certains évènements précédants 1939 l'ont annoncé auparavant.");
		Carte c2=creerCarte(1,"qcm","Francais","Quand a débuté la 2nd Guerre Mondiale?",new String[]{"1939","1938","1937","1940"},"La 2nd Guerre Mondiale à débuté officiellement en 1939, même si certains évènements précédants 1939 l'ont annoncé auparavant.");
		Carte c3=creerCarte(1,"qcm","Francais","Quand a débuté la 1ere Guerre Mondiale?",new String[]{"1914","1913","1915","1916"},"Le debut de la 1er Guerre Mondiale date de 1914 en Europe. Pus tard, en 1917, les Etats Unis d'amérique les rejoindrons");
		assertTrue(equals(c1,c2));
		assertFalse(equals(c1,c3));
		assertFalse(equals(c2,c3));
	}

	boolean equals(Carte c1,Carte c2){
		if((c1 == null && c2 == null) || c1 == c2){
			return true;
		}else if(c1 == null ^ c2 == null){
			return false;
		}
		return c1.difficulte == c2.difficulte && equals(c1.mode, c2.mode) && equals(c1.matiere, c2.matiere) && equals(c1.contenu, c2.contenu) && equals(c1.reponses, c2.reponses) && equals(c1.explication, c2.explication);
	}

	// Constructeur pour créer une carte

	void testCreerCarte(){
		Carte carte=creerCarte(1,"qcm","Francais","Quand a débuté la 2nd Guerre Mondiale?",new String[]{"1939","1938","1937","1940"},"La 2nd Guerre Mondiale à débuté officiellement en 1939, même si certains évènements précédants 1939 l'ont annoncé auparavant.");
		assertEquals(1,carte.difficulte);
		assertEquals("qcm",carte.mode);
		assertEquals("Francais",carte.matiere);
		assertEquals("Quand a débuté la 2nd Guerre Mondiale?",carte.contenu);
		assertArrayEquals(new String[]{"1939","1938","1937","1940"},carte.reponses);
		assertEquals("La 2nd Guerre Mondiale à débuté officiellement en 1939, même si certains évènements précédants 1939 l'ont annoncé auparavant.",carte.explication);
	}
	
	Carte creerCarte(int difficulte,String mode,String matiere,String contenu,String[] reponses,String explication){
		Carte c = new Carte();
		c.difficulte = difficulte;
		c.mode = mode;
		c.matiere = matiere;
		c.contenu = contenu;
		c.reponses = reponses;
		c.explication = explication;
		return c;
	}

	// Fonction pour générer toute les cartes possibles a partir d'un fichier csv contenu les question
	void testGenererCartes(){
		String[][] jeanMichel=new String[][]{{"1","int","Mathématiques","1+1=?","2","1+1=2, c'est mathématique."},{"1","qcm","Francais","Qui a écrit les misérables?","Victor Hugo|Molière|Albert Camus","Victor Hugo a écrit les misérables. Camus a écrit nottament l'Etranger et Molière Tartuffe."}};
		saveCSV(jeanMichel,"test.csv",';');
		ListCartes tab = genererCartes(loadCSV("test.csv",';'));
		Carte c=creerCarte(1,"int","Mathématiques","1+1=?",new String[]{"2"},"1+1=2, c'est mathématique.");
		assertTrue(equals(get(tab,0),c));
	}

	ListCartes genererCartes(CSVFile fichierSource) {
		ListCartes cartes = creerListCartes();
		for (int i = 0; i < rowCount(fichierSource); i++) {
				add(cartes,creerCarte(genererInformationsCarte(fichierSource, i)));
		}
		return cartes;
	}

	// Fonction qui génére une carte a partir d'une ligne d'un fichier CSV
        
	void testGenererInformationsCarte(){
		String[][] jeanMichel=new String[][]{{"1","int","Mathématiques","1+1=?","2","1+1=2, c'est mathématique."},{"1","qcm","Francais","Qui a écrit les misérables?","Victor Hugo|Jean Paul|Albert Camus","Victor Hugo a écrit les misérables. Camus a écrit nottament l'Etranger et Molière Tartuffe."}};
		saveCSV(jeanMichel,"test.csv",';');
		CSVFile test=loadCSV("test.csv",';');
		assertArrayEquals(new String[]{"1","int","Mathématiques","1+1=?","2","1+1=2, c'est mathématique."},genererInformationsCarte(test,0));
		assertArrayEquals(new String[]{"1","qcm","Francais","Qui a écrit les misérables?","Victor Hugo|Jean Paul|Albert Camus","Victor Hugo a écrit les misérables. Camus a écrit nottament l'Etranger et Molière Tartuffe."},genererInformationsCarte(test,1));
	}

	String[] genererInformationsCarte(CSVFile source, int line) {
			String[] result = new String[columnCount(source, line)];
			for (int i = 0; i < columnCount(source, line); i++) {
					result[i] = getCell(source, line, i);
			}
			return result;
	}

	// Fonction qui retourne un String contenant les informations sur un tableau de cartes
       
	void testCartesToString(){
		Carte[] c=new Carte[]{creerCarte(1,"qcm","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle","Un carée","Un cercle"},"C'est la définition même d'un triangle."),creerCarte(1,"string","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle"},"C'est la définition même d'un triangle.")};
		assertEquals("Difficulté : 1\nMode : qcm\nMatière : Mathématique\nContenu : Qu'est ce qu'une forme géométrique a 3 côtés?\nRéponse(s) : \n\t1 - Un triangle\n\t2 - Un carée\n\t3 - Un cercle\nExplication : C'est la définition même d'un triangle.\n\nDifficulté : 1\nMode : string\nMatière : Mathématique\nContenu : Qu'est ce qu'une forme géométrique a 3 côtés?\nRéponse(s) : Un triangle\nExplication : C'est la définition même d'un triangle.\n\n",toString(c));
	}

	String toString(Carte[] cartes) {
			String result = "";
			for (int i = 0; i < length(cartes); i++) {
					result = result + toString(cartes[i]);
			}
			return result;
	}

	// Fonction qui retourne un String contenant les informations sur une carte
        
	void testCarteToString(){
		Carte c=creerCarte(1,"qcm","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle","Un carée","Un cercle"},"C'est la définition même d'un triangle.");
		assertEquals(toString(c),"Difficulté : 1\nMode : qcm\nMatière : Mathématique\nContenu : Qu'est ce qu'une forme géométrique a 3 côtés?\nRéponse(s) : \n\t1 - Un triangle\n\t2 - Un carée\n\t3 - Un cercle\nExplication : C'est la définition même d'un triangle.\n\n");
	}

	String toString(Carte carte) {
			String reponses;
			if (equals(carte.mode, "qcm")) {
					reponses = "";
					for (int i = 0; i < length(carte.reponses); i++) {
							reponses = reponses + "\n\t" + (i + 1) + " - " + carte.reponses[i];
					}
			} else {
					reponses = carte.reponses[0];
			}
	
			return "Difficulté : " + carte.difficulte
					+ "\nMode : " + carte.mode 
					+ "\nMatière : " + carte.matiere
					+ "\nContenu : " + carte.contenu
					+ "\nRéponse(s) : " + reponses
					+ "\nExplication : " + carte.explication
					+ "\n\n";
	}
        
	// Triage des cartes

	void testTrierCategorieCarte(){
		ListCartes l=creerListCartes();
		Carte c1=creerCarte(1,"qcm","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle","Un carée"},"C'est la définition même d'un triangle.");
		Carte c2=creerCarte(2,"qcm","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle","Un carée","Un cercle"},"C'est la définition même d'un triangle.");
		Carte c3=creerCarte(1,"string","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle"},"C'est la définition même d'un triangle.");
		add(l, c1);
		add(l, c2);
		add(l, c3);
		ListCartes l2=trierCategorieCarte(l,CategorieCarte.DIFFICULTE,"1");
		assertTrue(contains(c1,l2));
		assertFalse(contains(c2,l2));
		assertTrue(contains(c3,l2));

	}
        
	ListCartes trierCategorieCarte(ListCartes cartes, CategorieCarte facteur, String comparateur) {
			ListCartes result = creerListCartes();
			
			for (int i = 0; i < length(cartes); i++) {
					if (facteur == CategorieCarte.DIFFICULTE) {
							int comp = motToInt(comparateur);
							if (get(cartes,i).difficulte == comp) {
									add(result,get(cartes,i));
							}
					} else if (facteur == CategorieCarte.MODE) {
							if (get(cartes,i).mode == comparateur) {
									add(result,get(cartes,i));
							}
					} else if (facteur == CategorieCarte.MATIERE) {
							if (equals(get(cartes,i).matiere,comparateur)) {
									add(result,get(cartes,i));
							}
					} else if (facteur == CategorieCarte.QUESTION) {
							if (get(cartes,i).contenu == comparateur) {
									add(result,get(cartes,i));
							}
					} else {        // facteur == reponse
							break;
					}
			}
			
			return result;
	}

	// Fonction qui crée une carte a partir d'une liste de String
        
	void testCreerCarteInfo(){
		Carte c = creerCarte(new String[]{"1","qcm","Francais","Quel est la bonne orthographe?","Français|Francais","Pour que la sonorité a l'orale correspondent a l'écriture, il ne faut pas oublier la cédille sur le c."});
		assertEquals(1,c.difficulte);
		assertEquals("qcm",c.mode);
		assertEquals("Francais",c.matiere);
		assertEquals("Quel est la bonne orthographe?",c.contenu);
		assertEquals("Pour que la sonorité a l'orale correspondent a l'écriture, il ne faut pas oublier la cédille sur le c.",c.explication);
		assertTrue(equals(c.reponses,new String[]{"Français","Francais"}));
	}

	Carte creerCarte(String[] informations) {
		Carte result = new Carte();
		result.difficulte = motToInt(informations[0]);
		result.mode = informations[1];
		result.matiere = informations[2];
		result.contenu = informations[3];
		result.reponses = splitReponsesCartes(informations[4]);
		result.explication = informations[5];
		return result;
	}


	// FONCTIONS SUR LES JOUEURS

	// Creer un joueur

	void testCreerJoueur(){
		ListCartes l = creerListCartes();
		add(l,creerCarte(1,"qcm","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle","Un carée","Un cercle"},"Un triangle a bien 3 côtés. Au contraire, un carrée a 4 côtés et un cercle une infinité de côté."));
		add(l,creerCarte(1,"string","Mathématique","Qu'est ce qu'une forme géométrique a 3 côtés?",new String[]{"Un triangle"},"C'est la définition même d'un triangle."));
		Joueur j=creerJoueur("Hugo",l,0);
		assertFalse(j.estIa);
		assertEquals("Hugo",j.nom);
		//assertTrue(equals(l,j.cartes));
		assertEquals(0,j.points);
	}

	Joueur creerJoueur(String nom,ListCartes cartes,int point){
		Joueur j = new Joueur();
		j.estIa = false;
		j.nom = nom;
		j.cartes = cartes;
		j.points = point;
		j.carteChoisie = null;
		j.cible = null;
		j.competences = creerListCompetences();
		return j;
	}

	// Affiche les infos sur un joueur
	
	String toString(Joueur j){
		return "Joueur: " + j.nom + "\nIA?: " + j.estIa+"\nCartes: " + toString(j.cartes) + "\nPoints: " + j.points + "\nCompetences:\n" + toString(j.competences);
	}

	// Verifie si deux joueurs sont egaux ou non

	void testEqualsJoueur(){
		Joueur j1=creerJoueur("Hugo",creerListCartes(),0);
		Joueur j2=creerJoueur("Gireg",creerListCartes(),0);
		Joueur j3=creerJoueur("Hugo",creerListCartes(),0);
		assertTrue(equals(j1,j3));
		assertFalse(equals(j1,j2));
	}

	boolean equals(Joueur j1, Joueur j2) {
		if((j1 == null && j2 == null) || j1 == j2){
			return true;
		}else if(j1 == null ^ j2 == null){
			return false;
		}
		else{
			return equals(j1.nom, j2.nom) && equals(j1.cartes, j2.cartes) && j1.points == j2.points && equals(j1.competences,j2.competences);
		}
	}

	/// FONCTIONS SUR LES COMPETENCE

	// Fonction pour créer une competence

	void testCreerCompetence(){
		Competence c=creerCompetence("Maths",0.5);
		assertTrue(equals("Maths", c.matiere));
		assertTrue(0.5==c.taux);
	}

	Competence creerCompetence(String matiere,double taux){
		Competence c = new Competence();
		c.matiere = matiere;
		c.taux = taux;
		return c;
	}

	// Fonction qui indique si deux competences sont égaux ou non
	
	void testEqualsCompetence(){
		Competence c1=creerCompetence("Maths",0.5);
		Competence c2=creerCompetence("Maths",0.5);
		Competence c3=creerCompetence("Maths",0.4);
		Competence c4=creerCompetence("Sciences",0.5);
		assertTrue(equals(c1, c2));
		assertFalse(equals(c1, c3));
		assertFalse(equals(c1, c4));
	}

	boolean equals(Competence c1,Competence c2){
		if(c1 == null ^ c2 == null){
			return false;
		}else if(c1 == null || c1 == c2){
			return true;
		}
		return equals(c1.matiere, c2.matiere) && c1.taux == c2.taux;
	}

	// Fonction qui retourne les informations d'une competence

	String toString(Competence c){
		return "Matière : "+c.matiere+"\nTaux : "+c.taux+"\n";
	}

	// Fonction qui retourne une liste de competence (sur toutes les matières des cartes utilisé) et met des taux de succès a 0.5

	ListCompetences creerListCompetencesInitiale(){
		ListCompetences res = creerListCompetences();
		for(int i = 0; i < length(toutesLesCartes); i++){
			String matiere = get(toutesLesCartes, i).matiere;
			if(!findBool(res, matiere)){
				add(res, creerCompetence(matiere, 0.5));
			}
		}
		return res;
	}

	// Fonction qui retourne le nom de la matière pour leuquel un joueur a le taux le plus faible

	void testCompetenceMin(){
		Joueur j = creerJoueur("Hugo",creerListCartes(),0);
		j.competences = creerListCompetences();
		add(j.competences, creerCompetence("Mathématiques", 0.25));
		add(j.competences, creerCompetence("Sciences", 0.2));
		add(j.competences, creerCompetence("Francais", 0.4));
		ListCartes l = creerListCartes();
		add(l, creerCarte(1,"qcm","Mathématique","Quel triangle particuler n'existe pas?",new String[]{"Le triangle carrée","Le triangle rectangle","Le triangle équilatéral","Le triangle isocèle"},"Le triangle carrée n'existe pas. Au contraire, un triangle rectangle est un triangle avec un angle droit, un triangle équilatéral est un triangle avec 3 côtés de même longeur et un triangle isocèle un triangle avec deux côtés de même longeur.;"));
		add(l, creerCarte(1,"qcm","Sciences","Quel est l'état de la matière qui n'existe pas?",new String[]{"L'état affalé","L'état liquide","L'état solide","L'état gazeux"},"L'état affalé n'existe pas en physique. Les trois autres propositions sont des états existants.;"));
		assertEquals("Sciences",competenceMin(j,l));
		remove(l,1);
		assertEquals("Mathématiques",competenceMin(j,l));
	}

	String competenceMin(Joueur j, ListCartes listCarte){
		String matiere = get(listCarte, 0).matiere;
		for(int i = 1; i < length(listCarte); i++){
			String nouvMatiere = get(listCarte, i).matiere;
			if(taux(j, nouvMatiere) < taux(j, matiere) || (random() > 0.5 && taux(j, nouvMatiere) == taux(j, matiere))){
				matiere = nouvMatiere;
			}
		}
		return matiere;
	}

	// Fonction qui indique si la competence d'une matière est déjà présent ou non dans une liste
	void testFindBool(){
		ListCompetences l= creerListCompetences();
		add(l, creerCompetence("Mathématiques", 0.25));
		add(l, creerCompetence("Sciences", 0.2));
		add(l, creerCompetence("Francais", 0.4));
		assertTrue(findBool(l,"Mathématiques"));
		assertFalse(findBool(l,"Anglais"));
	}

	boolean findBool(ListCompetences list,String matiere){
		int idx=0;
		while(idx < length(list)){
			if(equals(get(list, idx).matiere, matiere)){
				return true;
			}
			idx = idx + 1;
		}
		return false;
	}

	// Fonction qui retourne la competence correspondant a une matière dans une lsite

	void testFind(){
		ListCompetences l= creerListCompetences();
		Competence math=creerCompetence("mathématiques", 0.25);
		Competence sciences=creerCompetence("sciences", 0.25);
		add(l, creerCompetence("mathématiques", 0.25));
		add(l, creerCompetence("sciences", 0.2));
		add(l, creerCompetence("francais", 0.4));
		assertTrue(equals(math,find(l,"mathématiques")));
		assertTrue(equals(sciences,find(l,"sciences")));
	}

	Competence find(ListCompetences list,String matiere){
		int idx = 0;
		while(idx < length(list)){
			if(equals(get(list, idx).matiere, matiere)){
				return get(list, idx);
			}
			idx = idx + 1;
		}
		return null;
	}

	// Fonction qui retourne le taux d'une competence donnée

	double taux(Joueur joueur,String matiere){
		int idx = 0;
		ListCompetences list = joueur.competences;
		while(idx < length(list)){
			if(equals(get(list, idx).matiere, matiere)){
				return get(list, idx).taux;
			}
			idx = idx + 1;
		}
		return 0.5;
	}

	// FONCTIONS SUR LES IA
	
	// Créer une IA

	Joueur creerIA(String nom, ListCartes banqueCartes, int points) {
		Joueur ia = new Joueur();
		ia.estIa = true;
            	ia.nom = nom;
		ia.cartes = banqueCartes;
		ia.points = points;
		ia.carteChoisie = null;
		ia.cible = null;
		ia.competences=creerListCompetencesInitiale();
		for(int i=0;i<length(ia.competences);i++){
			get(ia.competences,i).taux=random();
		}
        	return ia;
	}

	// FONCTIONS SUR LES LISTES

	// Créer une liste vide

	void testCreerListCartes(){
		ListCartes l = creerListCartes();
		assertTrue(l.carte==null);
		assertTrue(l.suivante==null);
	}
	
	ListCartes creerListCartes() {
		ListCartes l = new ListCartes();
		l.carte = null;
		l.suivante = null;
		return l;
	}

	void testCreerListJoueurs(){
		ListJoueurs l = creerListJoueurs();
		assertTrue(l.joueur==null);
		assertTrue(l.suivant==null);
	}

	ListJoueurs creerListJoueurs() {
		ListJoueurs l = new ListJoueurs();
		l.joueur = null;
		l.suivant = null;
		return l;
	}

	ListCompetences creerListCompetences(){
		ListCompetences l = new ListCompetences();
		l.competence = null;
		l.suivante = null;
		return l;
	}

	// Verifie si une liste est vide ou non

	void testEstVideListCartes(){
		ListCartes l = creerListCartes();
		assertTrue(estVide(l));
	}

	boolean estVide(ListCartes l){
		return l.carte == null && l.suivante == null;
	}

	void testEstVide(){
		ListJoueurs l = creerListJoueurs();
		assertTrue(estVide(l));
	}

	boolean estVide(ListJoueurs l) {
		return l.joueur == null && l.suivant == null;
	}

	boolean estVide(ListCompetences l){
		return l.competence == null && l.suivante == null;
	}

	// Ajoute un élément a une liste

	void testAddCarte(){
		ListCartes l = creerListCartes();
		Carte c=creerCarte(1,"qcm","Test","Question",new String[]{"Reponse A","Reponse B","Reponse C"},"Explication lambda.");
		add(l,c);
		assertTrue(equals(c,l.carte));
		assertTrue(estVide(l.suivante));

	}

	void add(ListCartes l, Carte c) {
		if (estVide(l)){
			l.carte = c;
			l.suivante = creerListCartes();
		} else {
			add(l.suivante, c);
		}
	}

	void testAddJoueur(){
		ListJoueurs l = creerListJoueurs();
		Carte c=creerCarte(1,"qcm","Test","Question",new String[]{"Reponse A","Reponse B","Reponse C"},"Explication lambda.");
		Joueur j = creerJoueur("Hugo",creerListCartes(),1);
		add(l,j);
		assertTrue(equals(j,l.joueur));
		assertTrue(estVide(l.suivant));
	}

	void add(ListJoueurs l, Joueur j) {
		if (estVide(l)) {
			l.joueur = j;
			l.suivant = creerListJoueurs();
		} else {
			add(l.suivant, j);
		}
	}

	void add(ListCompetences l, Competence c){
		if (estVide(l)){
			l.competence = c;
			l.suivante = creerListCompetences();
		} else {
			add(l.suivante, c);
		}
	}

	// Retourne sous la forme d'un string les informations d'une liste

	String toString(ListCartes l) {
		if (estVide(l)) {
			return "";
		} else {
			return toString(l.carte) + toString(l.suivante);
		}
	}

	String toString(ListJoueurs l) {
		if (estVide(l)) {
			return "";
		} else {
			return toString(l.joueur) + toString(l.suivant);
		}
	}

	String toString(ListCompetences l){
		if (estVide(l)){
			return "";
		}else{
			return toString(l.competence) + toString(l.suivante);
		}
	}

	// Retourne la longeur d'une liste

	void testLengthListCartes(){
		ListCartes l = creerListCartes();
		add(l,creerCarte(1,"qcm","Test","Question",new String[]{"Reponse A","Reponse B","Reponse C"},"Explication lambda."));
		assertEquals(1,length(l));
		add(l,creerCarte(2,"int","Mathématiques","1+1=?",new String[]{"2"},"1+1=2, c'est mathématique."));
		assertEquals(2,length(l));

	}

	int length(ListCartes l) {
		return length(l, 0);
	}

	int length(ListCartes l, int status) {
		if (estVide(l)) {
			return status;
		} else {
			return length(l.suivante, status + 1);
		}
	}

	int length(ListJoueurs l) {
		return length(l, 0);
	}

	int length(ListJoueurs l, int status) {
		if (estVide(l)) {
			return status;
		} else {
			return length(l.suivant, status + 1);
		}
	}

	int length(ListCompetences l){
		return length(l,0);
	}

	int length(ListCompetences l, int status){
		if (estVide(l)){
			return status;
		} else{
			return length(l.suivante, status + 1);
		}
	}

	// Obtenir l'élément d'une liste a un certain index

	void testGetListCartes(){
		ListCartes l = creerListCartes();
		add(l,creerCarte(1,"qcm","Test","Question",new String[]{"Reponse A","Reponse B","Reponse C"},"Explication lambda"));
		add(l,creerCarte(2,"int","Mathématiques","1+1=?",new String[]{"2"},"1+1=2, c'est mathématique."));
		assertTrue(equals(creerCarte(2,"int","Mathématiques","1+1=?",new String[]{"2"},"1+1=2, c'est mathématique."),get(l,1)));
	}

	Carte get(ListCartes l, int i) {
		if (i >= 0) {
			return get(l, i, 0);
		} else {
			return null;
		}
	}

	Carte get(ListCartes l, int i, int count) {
		if (estVide(l) || count == i) {
			return l.carte;
		} else {
			return get(l.suivante, i, count + 1);
		}
	}

	Joueur get(ListJoueurs l, int i) {
		if (i >= 0) {
			return get(l, i, 0);
		} else {
			return null;
		}
	}

	Joueur get(ListJoueurs l, int i, int count) {
		if (estVide(l) || count == i) {
			return l.joueur;
		} else {
			return get(l.suivant, i, count + 1);
		}
	}

	Competence get(ListCompetences l, int i){
		if (i >=0) {
			return get(l,i,0);
		}else{
			return null;
		}
	}

	Competence get(ListCompetences l,int i,int count){
		if(estVide(l) || count == i){
			return l.competence;
		}else{
			return get(l.suivante, i, count + 1);
		}
	}

	// Retire d'une liste un élément a un certain indice donnée

	void testRemoveListCartes(){
		ListCartes l = creerListCartes();
		Carte c1=creerCarte(1,"int","Mathématiques","1+1=?",new String[]{"2"},"1+1=2, c'est mathématique.");
		add(l,c1);
		Carte c2=creerCarte(2,"qcm","Test","Question",new String[]{"Reponse A","Reponse B","Reponse C"},"Explication lambda.");
		add(l,c2);
		Carte c3=creerCarte(3,"qcm","Test","Question",new String[]{"Reponse A","Reponse B","Reponse C"},"Explication lambda.");
		add(l,c3);
		remove(l,1);
		assertTrue(equals(creerCarte(3,"qcm","Test","Question",new String[]{"Reponse A","Reponse B","Reponse C"},"Explication lambda."),get(l,1)));
	}

	void remove(ListCartes l, int i) {
		if (i >= 0 && i < length(l)) {
			remove(l, i, 0);
		}
	}

	void remove(ListCartes l, int i, int count) {
		if (i == count) {
			l.carte = l.suivante.carte;
			l.suivante = l.suivante.suivante;
		} else {
			remove(l.suivante, i, count + 1);
		}
	}

	void remove(ListJoueurs l, int i) {
		if (i >= 0 && i < length(l)) {
			remove(l, i, 0);
		}
	}

	void remove(ListJoueurs l, int i, int count) {
		if (i == count) {
			l.joueur = l.suivant.joueur;
			l.suivant = l.suivant.suivant;
		} else {
			remove(l.suivant, i, count + 1);
		}
	}

	void remove(ListCompetences l,int i){
		if(i>=0 && i<length(l)){
			remove(l,i,0);
		}
	}

	void remove(ListCompetences l,int i,int count){
		if (i == count){
			l.competence = l.suivante.competence;
			l.suivante = l.suivante.suivante;
		}else{
			remove(l.suivante,i,count + 1);
		}
	}

	// Retourne un élément de la liste choisi au hasard

	Joueur random(ListJoueurs list){
		return get(list,(int)(random()*length(list)));
	}

	Carte random(ListCartes list){
		return get(list,(int)(random()*length(list)));
	}

	Competence random(ListCompetences list){
		return get(list,(int)(random()*length(list)));
	}

	// Verifie si deux listes sont egaux ou non

	boolean equals(ListJoueurs j1, ListJoueurs j2) {
		if (length(j1) != length(j2)){
			return false;
		}else {
			if (equals(j1.joueur, j2.joueur)) {
				if(j1.suivant!=null){
					return equals(j1.suivant.joueur, j2.suivant.joueur);
				}else{
					return true;
				}
			} else {
				return false;
			}
		}
	}
	
	boolean equals(ListCartes c1, ListCartes c2) {
		if (length(c1) != length(c2)){
			return false;
		}
		else {
			if (equals(c1.carte, c2.carte)) {
				if(c1.suivante!= null){
					return equals(c1.suivante.carte, c2.suivante.carte);
				}else{
					return true;
				}
			} else {
				return false;
			}
		}
	}

	boolean equals(ListCompetences c1, ListCompetences c2){
		if(length(c1) != length(c2)){
			return false;
		}else{
			if(equals(c1.competence, c2.competence)){
				if(c1.suivante!=null){
					return equals(c1.suivante.competence,c2.suivante.competence);
				}else{
					return true;
				}
			}else{
				return false;
			}
		}
	}
}
