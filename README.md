# BluffoQuizz

Briatte Hugo (Groupe A) Lezoraine Gireg (Groupe A)

## Présentation de BluffoQuizz

**BluffoQuizz** est un jeu sur terminal qui se joue de 2 à 5 joueurs.
Le jeu se déroule tel quel. Chaque personne a des cartes avec des
questions écrites dessus. Le but est de gagner le plus de points
possible en un certain nombre de tours.

Pour cela, chacun son tour, un joueur donne une carte de son choix 
à un autre joueur. Celui-ci doit alors répondre à la question 
posée sous la forme d'un:
* QCM 
* ou bien d'une question à libre choix 
**Les joueurs gagnent des points soit en répondant correctement à la 
question, ou bien en donnant une carte à laquelle le joueur n'a pas su 
répondre. À l'inverse, ils perdent des points en répondant mal à 
une question ou bien en ayant donnée une carte à laquelle le joueur
a su répondre. Une boutique est présente pour acheter des cartes.**

Les questions porteront sur tous les sujets principaux de
l'école (Français,Mathématiques,Histoire-Géographie etc).
De plus, le fonctionnement du jeu incite les joueurs à faire travailler
à leurs amis les matières dans lesquels ils ont le plus de mal.
Cela garantit alors un apprentissage de l'élève.

## Utilisation de BluffoQuizz

Afin d'utiliser le projet, il doit être suffisant de taper les 
commandes suivantes:
./compile.sh            // lancer la compilation des fichiers
                        // présents dans 'src' et création des 
                        // fichiers '.class' dans 'classes'

Comme certains binômes ont un projet en mode texte et un autre 
en mode graphique, merci de préciser le nom des programmes à 
passer en paramètre au script 'run.sh'

./run.sh Main

Pour tous les projets: pensez à mettre l'ap.jar dans le répertoire 'lib' !

Pour les projets en mode graphiques, placez les images dans le répertoire
'ressources' sachant que son contenu est copié dans 'classes' lorsque 
vous faites un 'run'.

## Notes supplémentaires

Les fichiers CSV dans le dossier classes sont générés par les fonctions de tests
